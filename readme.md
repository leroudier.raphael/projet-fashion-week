# Projet Fashion-Week 

Autheurs : Thomas Aubert & Raphael Leroudier.

## Introduction

Le projet fashion week a pour objectifs:

1. De recopier un site existant de notre choix.
2. Proposer une version 2 modernisée.

la deadline est de deux jours pour ca réalisation complète.

## Composition

Ce repository comprend:

- un dossier *Version origin* contenant l'etape de reproduction du site
- un dssier *Version V2* contenant la seconde étape du projet.

## Réalisation

1. Dans ce projet nous avons décidés de recopier le site de Simplon Lyon : https://www.simplonlyon.fr/AccueilSimplon/

   Le site d'origine est codé en Flexbox, pour ca mise en page nous avons donc décider de le reproduire à l'identique avec Bootstrap seulement (tout en respectant le responsive).

2.  Pour cette seconde étape nous avons decidé de proposer une une version plus moderne en ce reposant sur une template pour le carousel principal et nous lui incorporons du boootstrap pour ca mise en page.

   



​         